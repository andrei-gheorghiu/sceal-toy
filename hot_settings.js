app.service('HotSettings', ['Helper',
    function HotSettings(Helper) {
        this.getSettings = function () {
            return {
                rowHeaders: ["Current"],
                colHeaders: [],
                colWidths: 90,
                rowHeights: 40,
                rowHeaderWidth: 175,
                autoRowSize: 0,
                autoColumnSize: 0,
                stretchH: "all",
                afterSelection: function (r, c, r2, c2, preventScrolling) {
                    //console.log(r, c, r2, c2);
                    //preventScrolling.value = true;
                }
            };
        };
    }]);