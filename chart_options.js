app.service('ChartOptions', ['$http', '$q',
    function ChartOptions($http, $q) {
        let that = this;
        this.getCommonSubtitle = function () {
            return {
                text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            }
        }
        this.getCommonAreaStops = function () {
            return [
                [0, Highcharts.getOptions().colors[0]],
                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
            ];
        }
        this.mergeOptionsWithValues = function (options, chartValues) {
            options.series[0].data = chartValues;
            return options;
        };
        this.addDraggable = function(options, dropFunction) {
            options.series[0].draggableY = true;
            let plotOptions = {
                series: {
                    point: {
                        events: {
                            drag: function (e) {
                                //console.log(this.series.name, this.category, e);
                            },
                            drop: dropFunction
                        }
                    },
                    stickyTracking: false
                }
            };
            options.plotOptions=plotOptions;
            return options;
        };
        //props { title:'', xType:'', yTitle:'', seriesName:'' }
        this.getOptions = function (optionId, props) {
            return $http.get('chart_options' + optionId + '.json')
                .then(function (success) {
                        let data = success.data;
                        data.subtitle = that.getCommonSubtitle();
                        data.plotOptions.area.stops = that.getCommonAreaStops();
                        data.title.text = props.title;
                        data.xAxis.type = props.xType;
                        data.yAxis.title.text = props.yTitle;
                        data.series.name = props.seriesName;
                        return data;
                    },
                    function (error) {
                        console.log(error);
                    });
        };
    }]);