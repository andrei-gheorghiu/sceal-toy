app.service('HotData', ['Helper',
    function HotData(Helper) {
        this.getItemsFromMetricData = function (metricData) {
            return [[...metricData.map(x => x.value)]];
        };
        this.getColumnsFromMetricData = function (metricData) {
            return metricData.map(function (x) {
                return {'title': Helper.formatHotDate(x.x)};
            });
        };
        this.getLastGoodValueFromMetricData = function (metricData) {
            for (let i = metricData.length-1; i > 0; i--) {
                if (metricData[i].value) return i;
            }
            return 0;
        }
    }]);