describe("Metric", function() {
  var moment=require("moment/moment");
  beforeEach(function() {
  });
  it("creates date from endpoint input correctly", function() {
      var date = 1438387200;
      //this test is not good as it will not work on diffetent time zone PC
      var currentTimeZone = moment.unix(date);
      //this should always work
      var utcTime = currentTimeZone.clone().utc();
      expect("2015-08-01T03:00:00+03:00").toEqual(currentTimeZone.format());
      expect("2015-08-01T00:00:00Z").toEqual(utcTime.format());
      expect(1438387200000).toEqual(utcTime.valueOf());
  });

});
