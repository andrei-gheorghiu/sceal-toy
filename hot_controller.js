app.controller('HotController', ['$scope', 'HotData', 'HotSettings', function HotController($scope, HotData, HotSettings) {
    let updateData = function (metricData) {
        $scope.items = HotData.getItemsFromMetricData(metricData);
        $scope.columns = HotData.getColumnsFromMetricData(metricData);
        $scope.lastGood = HotData.getLastGoodValueFromMetricData(metricData);
        //$scope.hotInstance.selectCell(0, $scope.lastGood-1);
    }
    let loadHotData = function () {
        $scope.hotdata.promise.then(
            function (success) {
                $scope.$watch(()=>$scope.$parent.originalData[$scope.hotdata.interval], updateData, true);
            },
            function (error) {
                console.log(error);
            }
        );
    }
    loadHotData();
    $scope.items = [[0]];
    $scope.settings = HotSettings.getSettings();
    $scope.settings.afterChange = function (change, source) {
        //console.log(change, source);
        $scope.$parent.updateData($scope.hotdata.interval, change);
    };
}]);