app.controller('MetricController', ['$scope', 'MetricData', 'Helper',
    function MetricController($scope, MetricData, Helper) {
        let dataLoaded=false;
        let getDataPromise = function (interval) {
            return MetricData.getData(interval).then(
                function (metricValues) {
                    dataLoaded=true;
                    if (!$scope.originalData) $scope.originalData = [];
                    $scope.originalData[interval] = metricValues.result[0].values;
                    //MetricData.addRandomMetricsDataInterval($scope.originalData[interval], interval, $scope, 1000);
                    return $scope.originalData[interval];
                }
            );
        };

        let updateData = function (interval, data) {
            if (!data) return;
            if(!dataLoaded) return;
            let metricData=$scope.originalData[interval];
            for (let i=0; i<data.length;i++) {
                metricData[data[i][1]].value=parseFloat(data[i][3]);
            }
            $scope.$digest();
            console.log("Save endpoint called...");
        };
        $scope.updateData = updateData;

        let updateDrag = function (interval, data) {
            if (!data) return;
            if(!dataLoaded) return;
            let metricData=$scope.originalData[interval];
            for (let i=0; i<metricData.length;i++) {
                if (metricData[i].x===data.x/1000) {
                    metricData[i].value=parseInt(data.y); //Highcharts.numberFormat(data.y, 2);
                    break;
                }
            }
            $scope.$digest();
            console.log("Save endpoint called...");
        };
        $scope.updateDrag = updateDrag;

        $scope.monthlyData=[];
        $scope.monthlyData.interval = 2592000;
        $scope.monthlyData.promise = getDataPromise($scope.monthlyData.interval);
        $scope.monthlyHot = "monthlyHot";
    }]);