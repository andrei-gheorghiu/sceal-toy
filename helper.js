app.service('Helper', ['$http',
    function Helper($http) {
        this.getRandomInt = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        this.formatHotDate = function (unixTs) {
            let date = moment.unix(unixTs);
            return date.month() + 1 + "-" + date.year();
        };
    }]);