app.directive('hcChart', [function () {
    return {
        restrict: 'C',
        template: '<div></div>',
        scope: {
            chartdata: '='
        },
        link: function (scope, element) {
        },
        controller: 'ChartController'
    };
}]);