app.directive('hotDirective', ['$compile', 'hotRegisterer', function ($compile, hotRegisterer) {
    return {
        restrict: 'C',
        compile: function (element, attrs) {
            return {
                pre: function (scope, element, attrs) {
                    element.append(
                        '<hot-table hot-id="'+ scope.hotid + '" settings="settings" datarows="items" columns="columns"></hot-table>'
                    );
                    scope.compiled=$compile(element.contents())(scope);
                },
                post: function(scope, iElem, iAttrs) {
                    scope.hotInstance=hotRegisterer.getInstance(scope.hotid);
                }
            }
        },
        scope: {
            hotdata: '=',
            hotid: '='
        },
        link: function (scope, element) {
        },
        controller: 'HotController'
    };
}]);