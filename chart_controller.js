app.controller('ChartController', ['$scope', '$element', 'ChartOptions',
    function ChartController($scope, $element, ChartOptions) {
        let rawToChart = function (rawValues) {
            return rawValues.map(function (p) {
                return [moment.unix(p.x).valueOf(), p.value]
            });
        };
        ChartOptions.getOptions(1, {
            title: 'Test metric',
            xType: 'datetime',
            yTitle: 'Test items',
            seriesName: 'test series'
        })
            .then(
                function (options) {
                    $scope.chartdata.promise.then(
                        function (metricValues) {
                            let chartValues=rawToChart(metricValues);
                            let draggableOptions=ChartOptions.addDraggable(options, function () {
                                $scope.$parent.updateDrag($scope.chartdata.interval, this);
                            });
                            let fullOptions = ChartOptions.mergeOptionsWithValues(draggableOptions, chartValues);
                            let chart = Highcharts.chart($element[0], fullOptions);
                            $scope.$watch(()=>$scope.$parent.originalData[$scope.chartdata.interval], function (newValue) {
                                $scope.items = rawToChart(newValue);
                                chart.series[0].setData($scope.items, true);
                            }, true);
                        }
                    );
                },
                function (error) {
                    console.log(error);
                }
            );
    }]);