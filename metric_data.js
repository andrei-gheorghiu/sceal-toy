app.service('MetricData', ['$http', 'Helper',
    function MetricData($http, Helper) {
        this.getData = function (metricId) {
            return $http.get('metric' + metricId + '.json')
                .then(function (success) {
                        return success.data;
                    },
                    function (error) {
                        console.log(error);
                    });
        };
        this.addRandomMetricsDataInterval = function (items, interval, scope, delay) {
            let x = items[items.length - 1].x;
            setInterval(
                function () {
                    x += interval;
                    items.push({
                        "x": x,
                        "value": Helper.getRandomInt(1, 15),
                        "is_reported": null,
                        "anomalies": [],
                        "comments": [],
                        "is_viewed": null
                    });
                    items.sort(function (a, b) {
                        return a.x - b.x;
                    });
                    scope.$digest();
                    //console.log(items);
                },
                delay
            )
        }
    }]);